package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 23/04/2018.
 */
public class Test01_Login extends BaseTest
{
    @Test
    public void testLogin()
    {
        //Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());

        //Step 2 click on the Login Link and the Login Page loads
        welcomePage.clickOnLogin();

        //Step 3 confirm that we're now on the login page
        assertTrue(loginPage.checkCorrectPage());

        //Step 4 Login with valid credentials
        loginPage.login("testuser","testing");

        //Step 5 Check that we're now on the User Page
        assertTrue(userPage.checkCorrectPage());

        //Step 6 click on logout link and the user logs out
        userPage.clickOnLogout();

        //Step 7 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
    }
}
